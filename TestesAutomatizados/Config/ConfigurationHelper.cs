﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestesAutomatizados.Config
{
    public class ConfigurationHelper
    {
        public static string SiteUrl => ConfigurationManager.AppSettings["SiteUrl"];

        public static string ChromeDrive => string.Format("{0}", ConfigurationManager.AppSettings["ChromeDrive"]);

        public static string TestUserName => ConfigurationManager.AppSettings["TestUserName"];

        public static string TestPassword => ConfigurationManager.AppSettings["TestPassword"];

        public static string FolderPath => Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));

        public static string FolderPicture => string.Format("{0}{1}", FolderPath, ConfigurationManager.AppSettings["FolderPicture"]);

        public static int BrowserType => Convert.ToInt32(ConfigurationManager.AppSettings["BrowserType"]);
    }
}
