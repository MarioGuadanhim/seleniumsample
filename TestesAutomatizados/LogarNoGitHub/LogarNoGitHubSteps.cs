﻿using System;
using TechTalk.SpecFlow;
using TestesAutomatizados.Config;
using Xunit;

namespace TestesAutomatizados.LogarNoGitHub
{
    [Binding]
    public class LogarNoGitHubSteps
    {
        public SeleniumHelper Browser;

        public LogarNoGitHubSteps()
        {
            Browser = SeleniumHelper.Instance();
        }


        [Given(@"que um usuario esta no site na pagina principal")]
        public void DadoQueUmUsuarioEstaNoSiteNaPaginaPrincipal()
        {
            var url = Browser.NavegarParaSite(ConfigurationHelper.SiteUrl);
            Assert.Equal(ConfigurationHelper.SiteUrl, url);
        }

        [Given(@"preenche os campos")]
        public void DadoClicaNoBotaoDeLogin(Table table)
        {
            Browser.PreencherTextBoxPorName("login", table.Rows[0][1]);
            Browser.PreencherTextBoxPorName("password", table.Rows[1][1]);
        }

        [When(@"clicar no botão login")]
        public void DadoPreencheOsCampos()
        {
            Browser.ClicarNoBotaoPorName("commit");
        }
        
        
        [Then(@"Uma mensagem de erro aparece")]
        public void EntaoERedirecionadoParaPaginaPrincipalDoSite()
        {
            var returnText = Browser.ObterTextoElementoPorId("js-flash-container");
            Assert.Contains("Incorrect username or password.", returnText);
            Browser.ObterScreenShot("EvidenciaErro");
        }
    }
}
