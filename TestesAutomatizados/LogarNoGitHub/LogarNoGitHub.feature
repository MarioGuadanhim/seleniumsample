﻿Funcionalidade: Logar no git hub
Um usuario deve preencher os campos necessarios
do formulario de login do site git hub 
e ao terminar será reencaminhado para o site ou recebera
uma mensagem de erro

@TesteAutomatizadoLoginNoGitHub

Cenário: Logar no git sem sucesso
Dado que um usuario esta no site na pagina principal
E preenche os campos
| Campo    | Valor      |
| name     | usuarioGit |
| password | senhaGit   |
Quando clicar no botão login
Entao Uma mensagem de erro aparece